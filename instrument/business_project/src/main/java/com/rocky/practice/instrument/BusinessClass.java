package com.rocky.practice.instrument;

import java.util.Date;

/**
 * @author pengqingsong
 * @date 2021/8/12
 * @desc
 */
public class BusinessClass {

    public void businessMethod() {
        System.out.println("businessMethod code: " + new Date());
    }


    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            new BusinessClass().businessMethod();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {

            }
        }
    }
}
